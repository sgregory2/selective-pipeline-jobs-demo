# frozen_string_literal: true

# problem: need to filter tests __before__ orchestration
# and trigger the job that it belongs to dynamically
require 'extended_dir'
require 'theorem'
require 'rspec/expectations'
require_relative "#{__dir__}/tests/base.rb"

require 'yaml'

# get test classes
ExtendedDir.require_all './tests/scenarios'

# get meta data
labels = ENV['CI_MERGE_REQUEST_LABELS']&.split(',')
all_test_classes = Theorem::Hypothesis.registry

test_jobs = all_test_classes.each_with_object([]) do |test_class, jobs_to_run|
  a = test_class.tests.inject([]) do |local_jobs, test|
    test_labels = test.metadata[:labels] || []

    local_jobs.concat test.metadata[:jobs] if (labels & test_labels).size.positive? && test.metadata[:jobs].is_a?(Array)
    local_jobs
  end

  jobs_to_run.concat a
end.uniq

exit 0 unless test_jobs.any?

jobs_hash = test_jobs.each_with_object({}) do |job_name, hash|
  hash[job_name] = {
    'script' => ['bundle install', 'bundle exec theorize --require ./harness.rb --harness Test::Harness'],
    'variables' => { 'LABELS_TO_RUN' => labels.join(',') },
    'image' => 'ruby:latest',
    'stage' => 'test',
    'when' => 'always'
  }
end

hash = {
  'workflow' => {
    'rules' => [ { 'if' => '$CI_MERGE_REQUEST_ID' }]
  }
}.merge(jobs_hash)

File.open('children.yml', 'w') do |f|
  f << hash.to_yaml
end


