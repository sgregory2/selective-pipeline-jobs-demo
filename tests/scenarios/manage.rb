# frozen_string_literal: true

require_relative '../base'

class Manage < Base
  test 'this job will trigger job_1 on the tag :manage', jobs: %w[job_1], labels: %w[manage integrations all] do
    expect(true).to be(true)
  end

  test 'this job will trigger job_1 on the tag :manage', jobs: %w[job_1], labels: %w[manage all] do
    expect(true).to be(true)
  end

  test 'no tags', labels: %w[all] do
    expect(true).to be(true)
  end
end