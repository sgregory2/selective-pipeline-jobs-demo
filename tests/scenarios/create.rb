# frozen_string_literal: true

require_relative '../base'

# example test class
class Create < Base
  test 'this job will trigger job_1 on the tag :create', jobs: %w[job_1], labels: %w[create integrations all] do
    expect(true).to be(true)
  end

  test 'this job will trigger job_1 on the tag :two', jobs: %w[job_1], labels: %w[create all] do
    expect(true).to be(true)
  end

  test 'no tags', labels: %w[all] do
    expect(true).to be(true)
  end
end