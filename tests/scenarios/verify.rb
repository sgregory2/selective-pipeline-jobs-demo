# frozen_string_literal: true

require_relative '../base'

class Verify < Base
  test 'this job will trigger job_3 on the tag :verify', jobs: %w[job_3], labels: %w[verify all] do
    expect(true).to be(true)
  end

  test 'this job will trigger job_3 on the tag :verify', jobs: %w[job_3], labels: %w[integrations verify all] do
    expect(true).to be(true)
  end
end