# frozen_string_literal: true

require_relative '../base'

class Package < Base
  test 'this job will trigger job_2 on the tag :package', jobs: %w[job_2], labels: %w[package all] do
    expect(true).to be(true)
  end

  test 'this job will trigger job_2 and job_3 on the tag :package', jobs: %w[job_2 job_3], labels: %w[package all] do
    expect(true).to be(true)
  end

  test 'will not run', labels: %w[all] do
    expect(true).to be(true)
  end
end