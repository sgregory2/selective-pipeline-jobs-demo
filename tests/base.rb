# frozen_string_literal: true

require 'rspec/expectations'

# base test class
class Base
  include Theorem::Hypothesis
  include RSpec::Matchers
end
