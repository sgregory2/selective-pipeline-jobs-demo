# frozen_string_literal: true

require 'extended_dir'
require 'yaml'
ExtendedDir.require_all './tests/scenarios'

module Test
  # Harness for running the tests
  # This will load all the files in ./test/scenarios
  # and filter the test registry to only run examples
  # that match the labels.
  module Harness
    include Theorem::Control::Harness

    load_tests do
      labels = ENV['LABELS_TO_RUN']&.split(',') || []

      # iterate through the test classes
      registry.each do |test_class|
        # mutate each test class to run only test it matches the labels
        # passed in through the environment
        test_class.tests.select! do |test|
          test_labels = test.metadata[:labels] || []

          (test_labels & labels).size.positive?
        end
      end

      registry
    end
  end
end

module Orchestrator
  # Harness for orchestrating dynamic child pipelines
  module Harness
    include Theorem::Control::Harness

    # instead of running the tests
    # fetch all the applicable jobs
    # and write job definitions for them
    on_run do |test_classes|
      labels = ENV['CI_MERGE_REQUEST_LABELS']&.split(',')

      all_applicable_jobs = test_classes.each_with_object([]) do |test_class, jobs_to_run|
        test_class.tests.each_with_object(jobs_to_run) do |test, test_jobs_to_run|
          test_labels = test.metadata[:labels] || []
          label_matches = (labels & test_labels).size.positive?

          test_jobs_to_run.concat test.metadata[:jobs] if label_matches && test.metadata[:jobs].is_a?(Array)
        end
      end

      Harness.write_jobs_to_yaml(all_applicable_jobs, labels)
      []
    end

    class << self
      def build_job_definitions(applicable_jobs, labels)
        applicable_jobs.each_with_object(pipeline_definition) do |job_name, definition|
          definition[job_name] = {
            'script' => [
              'bundle install',
              'bundle exec theorize --require ./harness.rb --harness Test::Harness -d ./tests/scenarios'
            ],
            'variables' => { 'LABELS_TO_RUN' => labels.join(',') },
            'image' => 'ruby:latest',
            'stage' => 'test',
            'when' => 'always'
          }
        end
      end

      def pipeline_definition
        {
          'workflow' => {
            'rules' => [ { 'if' => '$CI_MERGE_REQUEST_ID' }]
          }
        }
      end

      def write_jobs_to_yaml(all_applicable_jobs, labels)
        job_definitions = build_job_definitions(all_applicable_jobs, labels)

        File.open('children.yml', 'w') do |f|
          f << job_definitions.to_yaml
        end
      end
    end
  end
end
